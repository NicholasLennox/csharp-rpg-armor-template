﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ArmorClasses.Armor
{
    public abstract class ArmorAbstraction
    {
        public string Name { get; set; }
        public Stats ArmorStats { get; set; }
        public int Level { get; set; }
        public Slot ArmorSlot { get; set; }

        protected double scaling;

        protected ArmorAbstraction(string name, int level, Slot armorSlot)
        {
            Name = name;
            Level = level;
            ArmorSlot = armorSlot;
            scaling = (int)armorSlot;
            scaling /= 100;
        }
    }
}
