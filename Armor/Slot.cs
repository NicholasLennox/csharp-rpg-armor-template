﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ArmorClasses.Armor
{
    public enum Slot
    {
        Head = 60,
        Body = 100,
        Legs = 80
    }
}
