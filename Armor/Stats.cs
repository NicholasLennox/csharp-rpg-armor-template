﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ArmorClasses.Armor
{
    public class Stats
    {
        public int Health { get; set; }
        public int Dexterity { get; set; }
        public int Strength { get; set; }
        public int Intelligence { get; set; }
    }
}
