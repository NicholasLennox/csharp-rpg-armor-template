﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ArmorClasses.Armor
{
    public class Cloth : ArmorAbstraction
    {
        public Cloth(string name, int level, Slot armorSlot) : base(name,level,armorSlot)
        {
            ArmorStats = new Stats()
            {
                Health = (int)((20 + 5 * level) * scaling),
                Dexterity = (int)((8 + 2 * level) * scaling),
                Strength = (int)((5 + 1 * level) * scaling),
                Intelligence = (int)((10 + 3 * level) * scaling),
            };
        }
    }
}
