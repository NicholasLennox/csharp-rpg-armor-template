﻿using ArmorClasses.Armor;
using System;
using System.Collections.Generic;
using System.Text;

namespace ArmorClasses.Heroes
{
    public class Hero
    {
        public Dictionary<Slot, ArmorAbstraction> EquippedArmor { get; set; } = new Dictionary<Slot, ArmorAbstraction>();

        public void EquipArmor(ArmorAbstraction armor)
        {
            if (EquippedArmor.ContainsKey(armor.ArmorSlot))
            {
                EquippedArmor[armor.ArmorSlot] = armor;
            } else
            {
                EquippedArmor.Add(armor.ArmorSlot, armor);
            }
        }
    }
}
