﻿using System;
using ArmorClasses.Armor;
using ArmorClasses.Heroes;

namespace ArmorClasses
{
    class Program
    {
        static void Main(string[] args)
        {
            ArmorAbstraction itemHead = new Cloth("Cloth helm of examplis", 5, Slot.Head);
            ArmorAbstraction itemBody = new Cloth("Cloth robe of examplis", 5, Slot.Body);
            ArmorAbstraction itemLegs = new Cloth("Cloth pants of examplis", 5, Slot.Legs);
            ArmorAbstraction itemBodyReplace = new Cloth("Cloth notrobe of examplis", 5, Slot.Body);

            Hero hero = new Hero();

            hero.EquipArmor(itemHead);
            hero.EquipArmor(itemBody);
            hero.EquipArmor(itemLegs);
            hero.EquipArmor(itemBodyReplace);


        }
    }
}
